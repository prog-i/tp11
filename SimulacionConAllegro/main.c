/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: loloandrade
 *
 * Created on May 17, 2023, 11:32 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <allegro5/allegro.h> 
#include <allegro5/allegro_color.h> 
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h> 
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <stdbool.h>

#include "puerto.h"

#define D_WIDTH     520
#define D_HEIGHT    266
#define RADIO_CIRCULO 20
#define COLOR_INC_RATE 1.9
#define COLOR_DEC_RATE 2
#define MAX_COLOR_VALUE 255



int main(int argc, char** argv)     //Para el correcto funcionamiento del programa se debe presionar una tecla a la vez.
{
    int i;
    float x1,y1,x2,y2;
    float key;          
    unsigned int color = 1;
    int aux[8];         //Se utiliza para guardar el estado incial de los leds para realizar el parpadeo
    int f_1 = 0;        //Se utiliza para hacer la copia del estado inicial de los leds una sola vez para realizar el parpadeo
    int f_valido;       //Se utiliza para que no se haga flip display con cualquier tecla
    
    //EVENTOS//
    ALLEGRO_DISPLAY * display = NULL;
    ALLEGRO_EVENT_QUEUE * event_queue = NULL;
    ALLEGRO_TIMER *timer = NULL;
    ALLEGRO_FONT * font = NULL;
    ALLEGRO_SAMPLE *audio_sample_on = NULL;
    ALLEGRO_SAMPLE *audio_sample_off = NULL;
    ALLEGRO_SAMPLE *audio_sample_start = NULL;
    ALLEGRO_SAMPLE *audio_sample_toggle = NULL;
    ALLEGRO_SAMPLE *audio_sample_blinking = NULL;
    ALLEGRO_SAMPLE *audio_sample_shutdown = NULL;
    
    bool close_display = false;
    bool key_b = false;         //Se utiliza para verificar si se presiono la tecla b para controlar el parpadeo de los leds
    bool redraw = false;        //Se utiliza para realizar el parpadeo de los leds

    
    //INICIALIZACIONES//
    
    //Inicializacion de allegro
    if (!al_init())                         
    {
        fprintf(stderr, "Error al inicializar Allegro.\n");
        return -1;
    }

    //Inicializacion del addon de primitivas
    if (!al_init_primitives_addon())        
    {
        fprintf(stderr, "Error al inicializar el addon de primitivas.\n");
        return -1;
    }
    
    //Inicializacion del texto
    al_init_font_addon();                   //Inicializacion del addon de font
    al_init_ttf_addon();                    //Inicializacion del addon de ttf(True Type Font) 
    font = al_load_ttf_font("VCR_OSD_MONO_1.001.ttf", 68, 0); 
    if (!font) 
    {
        fprintf(stderr, "No se pudo cargar la letra 'VCR_OSD_MONO_1.001.ttf'.\n");
        return -1;
    }
    
    //Inicializacion del teclado
    if (!al_install_keyboard())             
    {
        fprintf(stderr, "Error al inicializar el teclado\n");
        return -1;
    }
    
    //Inicializacion del timer
    timer = al_create_timer(1.0);           
    if (!timer) 
    {
        fprintf(stderr, "Error al crear el timer\n");
        return -1;
    }

    //Inicializacion de la cola de eventos
    event_queue = al_create_event_queue();  
    if (!event_queue) 
    {
        fprintf(stderr, "Error al crear la cola de eventos\n");
        al_destroy_timer(timer);
        return -1;
    }  
    
    //Inicializacion del display
    display = al_create_display(D_WIDTH, D_HEIGHT); 
    if (!display)                           
    {
        fprintf(stderr, "Error al crear el display.\n");
        al_destroy_event_queue(event_queue);
        al_destroy_timer(timer);
        return -1;
    }
    al_set_window_title(display, "Port A");
    //al_set_window_position(display, 400, 300);
    
    
    //Inicializacion del audio
    if (!al_install_audio()) 
    {
        fprintf(stderr, "Error al instalar el audio.\n");
        al_destroy_event_queue(event_queue);
        al_destroy_display(display);
        return 1;
    }

    //Inicializacion del addon de los codecs de audio
    if (!al_init_acodec_addon()) 
    {
        fprintf(stderr, "Error al inicializar los codecs de audio.\n");
        al_destroy_event_queue(event_queue);
        al_destroy_display(display);
        return 1;
    }

    //Reserva de samples
    if (!al_reserve_samples(5)) 
    {
        fprintf(stderr, "Error al reservar las muestras de audio.\n");
        al_destroy_event_queue(event_queue);
        al_destroy_display(display);
        return 1;
    }

    audio_sample_on = al_load_sample("soundon.wav");
    if (!audio_sample_on) {
        fprintf(stderr, "Error al cargar el archivo de audio de bit encendido.\n");
        al_destroy_event_queue(event_queue);
        al_destroy_display(display);
        return 1;
    }

    audio_sample_off = al_load_sample("soundoff.wav");
    if (!audio_sample_off) 
    {
        fprintf(stderr, "Error al cargar el archivo de audio de bit apagado.\n");
        al_destroy_event_queue(event_queue);
        al_destroy_display(display);
        al_destroy_sample(audio_sample_on);
        return 1;
    }
    
    audio_sample_start = al_load_sample("startsound.wav");
    if (!audio_sample_start) 
    {
        fprintf(stderr, "Error al cargar el archivo de audio de inicio.\n");
        al_destroy_event_queue(event_queue);
        al_destroy_display(display);
        al_destroy_sample(audio_sample_on);
        return 1;
    }
    
    audio_sample_toggle = al_load_sample("togglesound.wav");
    if (!audio_sample_toggle) 
    {
        fprintf(stderr, "Error al cargar el archivo de audio de toggle.\n");
        al_destroy_event_queue(event_queue);
        al_destroy_display(display);
        al_destroy_sample(audio_sample_on);
        return 1;
    }
    
    audio_sample_blinking = al_load_sample("blinking.wav");
    if (!audio_sample_blinking) 
    {
        fprintf(stderr, "Error al cargar el archivo de audio de parpadeo.\n");
        al_destroy_event_queue(event_queue);
        al_destroy_display(display);
        al_destroy_sample(audio_sample_on);
        return 1;
    }
    
    audio_sample_shutdown = al_load_sample("shutdown.wav");
    if (!audio_sample_shutdown) 
    {
        fprintf(stderr, "Error al cargar el archivo de audio de cerrar.\n");
        al_destroy_event_queue(event_queue);
        al_destroy_display(display);
        al_destroy_sample(audio_sample_on);
        return 1;
    }
    
    //REGISTRO DE EVENTOS//
    
    al_register_event_source(event_queue, al_get_keyboard_event_source());          //registro el teclado
    al_register_event_source(event_queue, al_get_timer_event_source(timer));        //registro el timer
    al_register_event_source(event_queue, al_get_display_event_source(display));    //registro el display
    
    
    //PREPARACION DEL DISPLAY//
    
    al_play_sample(audio_sample_start, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
    
    while(color)    // animación "opening port"
    {
        al_clear_to_color(al_map_rgb(color, color, color));
        al_draw_text(font, al_map_rgb(color, 0, 0), (D_WIDTH / 2), (D_HEIGHT / 4), ALLEGRO_ALIGN_CENTER, "OPENING");
        al_draw_text(font, al_map_rgb(color, 0, 0), (D_WIDTH / 2), (D_HEIGHT / 2), ALLEGRO_ALIGN_CENTER, "PORT A");
        al_flip_display();
        color += COLOR_INC_RATE;        //Incrementa el color
        color %= MAX_COLOR_VALUE;       //Se queda con el resto de Max_color_rate
    }
    
    color = 255;
    
    al_clear_to_color(al_color_name("white"));   //Aplico color blanco al fondo para tapar el cartel de "Opening Port A"
    
    al_draw_filled_rectangle(20, 40, 500, 100, al_color_name("silver"));    //Fondo gris detras de los leds

    al_draw_rectangle(20, 40, 500, 100, al_color_name("black"), 2);         //Marco de puerto

    for(i=0,x1 = 80, y1 = 40, x2 = 80, y2 = 100; i < 7 ; i++)
    {
        al_draw_line(x1, y1, x2, y2, al_color_name("black"), 2);        //Separo 8 espacios para posicionar los leds
        x1 += 60;
        x2 += 60;
    }
   
    for(i=0,x1=50,y1=70; i < 8 ; i++)
    {
        al_draw_filled_circle(x1, y1, RADIO_CIRCULO, al_color_name("darkred")); //Dibujo los leds en su respectivo espacio. Color rojo oscuro para simular apagado
        al_draw_circle(x1, y1, RADIO_CIRCULO, al_color_name("black"), 2);       //Marco de leds
        x1 += 60;
    }
    
    font = al_load_ttf_font("VCR_OSD_MONO_1.001.ttf", 20, 0);       
    char const num[8][2]={"7","6","5","4","3","2","1","0"};     //cada string contiene su numero y su terminador
    for(i=0, x1=50, y1=10; i<8 ;i++)           
    {
        al_draw_text(font, al_color_name("black"), x1, y1, ALLEGRO_ALIGN_CENTER, &(num[i][0]));    //solo tomo la posicion con el numero en cada llamada a for
        x1 += 60;
    }
    
    font = al_load_ttf_font("VCR_OSD_MONO_1.001.ttf", 17, 0);
    al_draw_text(font, al_color_name("black"), 5, 120, ALLEGRO_ALIGN_LEFT, "Instrucciones de uso:");
    al_draw_text(font, al_color_name("black"), 5, 140, ALLEGRO_ALIGN_LEFT, "- Presione un n° de led para encenderlo / apagarlo.");
    al_draw_text(font, al_color_name("black"), 5, 160, ALLEGRO_ALIGN_LEFT, "- c: Apagar los leds encendidos.");
    al_draw_text(font, al_color_name("black"), 5, 180, ALLEGRO_ALIGN_LEFT, "- s: Encender los leds apagados.");
    al_draw_text(font, al_color_name("black"), 5, 200, ALLEGRO_ALIGN_LEFT, "- t: Apagar los encendidos y encender los apagados.");
    al_draw_text(font, al_color_name("black"), 5, 220, ALLEGRO_ALIGN_LEFT, "- b: Parpadeo de los leds encendidos.");
    al_draw_text(font, al_color_name("black"), 5, 240, ALLEGRO_ALIGN_LEFT, "- q: Cerrar el programa.");
    
    al_flip_display();   //aplico cambios al display
    
    
    //START TIMER//
    
    al_start_timer(timer);


    //EJECUCION//
    
    while (!close_display) 
    {      
        ALLEGRO_EVENT ev;
        key = -1;
        f_valido = 0;
        
        if (al_get_next_event(event_queue, &ev))        //Toma un evento de la cola
        {
            
            if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
            {
                al_play_sample(audio_sample_shutdown, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                    
                    font = al_load_ttf_font("VCR_OSD_MONO_1.001.ttf", 68, 0);
                    
                    while(color)    // animación "closing port"
                    {
                        al_clear_to_color(al_map_rgb(color, color, color));
                        al_draw_text(font, al_map_rgb(color, 0, 0), (D_WIDTH / 2), (D_HEIGHT / 4), ALLEGRO_ALIGN_CENTER, "CLOSING");
                        al_draw_text(font, al_map_rgb(color, 0, 0), (D_WIDTH / 2), (D_HEIGHT / 2), ALLEGRO_ALIGN_CENTER, "PORT");
                        al_flip_display();
                        color -= COLOR_DEC_RATE;        //Decrementa el color
                        color %= MAX_COLOR_VALUE;       //Se queda con el resto de Max_color_rate 
                    }
                    
                    close_display = true;                    //Salir del programa si se cierra la ventana
            }
 
            else if (ev.type == ALLEGRO_EVENT_TIMER) 
            {
                if (key_b)
                {
                    redraw = true;
                    
                    if (f_1)
                    {
                        for (i = 0; i < 8; i++)
                        {
                            if(bitGet('A', i))
                            {
                                aux[i] = bitGet('A', i);
                            }
                            else
                            {
                                aux[i] = 0;
                            }
                        }
                        
                        f_1 = 0;
                    } 
                }
            }
            
            else if (ev.type == ALLEGRO_EVENT_KEY_DOWN) 
            {
   
                if (ev.keyboard.keycode == ALLEGRO_KEY_B) 
                {
                    key_b = !key_b;
                    f_1 = 1;
                    f_valido = 1;
                    
                    if (key_b == false)
                    {
                        al_stop_samples();  // cuando se vuelve a tocar b, frena los samples (el loop de blinking.wav)
                    }
                    else
                    {
                        al_play_sample(audio_sample_blinking, 3.0, 0.0, 0.36, ALLEGRO_PLAYMODE_LOOP, NULL);     // loop del sample blinking.wav
                    }
                }

                else if (ev.keyboard.keycode == ALLEGRO_KEY_C)
                {
                    if(key_b == false)
                    {
                        for(i=0,x1=50,y1=70; i < 8 ; i++)
                        {
                            al_draw_filled_circle(x1, y1, RADIO_CIRCULO, al_color_name("darkred"));
                            al_draw_circle(x1, y1, RADIO_CIRCULO, al_color_name("black"), 2);
                            x1 += 60;
                            bitClr('A', i);
                        }

                        al_play_sample(audio_sample_off, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                        
                        f_valido = 1;
                    }
                }   
                    
                else if (ev.keyboard.keycode == ALLEGRO_KEY_Q)
                {
                    al_play_sample(audio_sample_shutdown, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                    
                    font = al_load_ttf_font("VCR_OSD_MONO_1.001.ttf", 68, 0);
                    
                    while(color)    // animación "closing port"
                    {
                        al_clear_to_color(al_map_rgb(color, color, color));
                        al_draw_text(font, al_map_rgb(color, 0, 0), (D_WIDTH / 2), (D_HEIGHT / 4), ALLEGRO_ALIGN_CENTER, "CLOSING");
                        al_draw_text(font, al_map_rgb(color, 0, 0), (D_WIDTH / 2), (D_HEIGHT / 2), ALLEGRO_ALIGN_CENTER, "PORT");
                        al_flip_display();
                        color -= COLOR_DEC_RATE;        //Decrementa el color
                        color %= MAX_COLOR_VALUE;       //Se queda con el resto de Max_color_rate 
                    }
                    
                    close_display = true; 
                }
                    
                else if (ev.keyboard.keycode == ALLEGRO_KEY_S)
                {
                    if(key_b == false)
                    {
                        for(i=0,x1=50,y1=70; i < 8 ; i++)
                        {
                            al_draw_filled_circle(x1, y1, RADIO_CIRCULO, al_color_name("red"));
                            al_draw_circle(x1, y1, RADIO_CIRCULO, al_color_name("black"), 2);
                            x1 += 60;
                            bitSet('A', i);
                        }

                        al_play_sample(audio_sample_on, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                        
                        f_valido = 1;
                    }
                }
                    
                else if (ev.keyboard.keycode == ALLEGRO_KEY_T)
                {
                    if(key_b == false)
                    {
                        for (i=0; i < 8 ; i++)
                        {
                            bitToggle('A', i);
                            if (bitGet('A', i))
                            {
                               al_draw_filled_circle(470 - (i*60), 70, RADIO_CIRCULO, al_color_name("red"));
                               al_draw_circle(470 - (i*60), 70, RADIO_CIRCULO, al_color_name("black"), 2);
                            }
                            else
                            {
                              al_draw_filled_circle(470 - (i*60), 70, RADIO_CIRCULO, al_color_name("darkred"));
                              al_draw_circle(470 - (i*60), 70, RADIO_CIRCULO, al_color_name("black"), 2);  
                            }
                        }    

                        al_play_sample(audio_sample_toggle, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                        
                        f_valido = 1;
                    }
                }
                
                else if (ev.keyboard.keycode == ALLEGRO_KEY_0)
                    key = 0;
                else if (ev.keyboard.keycode == ALLEGRO_KEY_1)
                    key = 1;
                else if (ev.keyboard.keycode == ALLEGRO_KEY_2)
                    key = 2;
                else if (ev.keyboard.keycode == ALLEGRO_KEY_3)
                    key = 3;
                else if (ev.keyboard.keycode == ALLEGRO_KEY_4)
                    key = 4;
                else if (ev.keyboard.keycode == ALLEGRO_KEY_5)
                    key = 5;
                else if (ev.keyboard.keycode == ALLEGRO_KEY_6)
                    key = 6;
                else if (ev.keyboard.keycode == ALLEGRO_KEY_7)
                    key = 7;
                                       
                if ((key != -1) && (key_b == false))
                {
                    bitToggle('A', key);
                    f_valido = 1;
                    if (bitGet('A', key))
                    {
                        al_draw_filled_circle(470 - (key*60), 70, RADIO_CIRCULO, al_color_name("red"));
                        al_draw_circle(470 - (key*60), 70, RADIO_CIRCULO, al_color_name("black"), 2);
                        
                        al_play_sample(audio_sample_on, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                    }
                    else
                    {
                        al_draw_filled_circle(470 - (key*60), 70, RADIO_CIRCULO, al_color_name("darkred"));
                        al_draw_circle(470 - (key*60), 70, RADIO_CIRCULO, al_color_name("black"), 2); 
                        
                        al_play_sample(audio_sample_off, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                    }
                }
                
                if(f_valido)
                    al_flip_display();
                
            }
            
            if (redraw && al_is_event_queue_empty(event_queue))
            {
                redraw = false;
           
                for (i = 0; i < 8; i++) 
                {
                    if ((bitGet('A', i) == 1) && (aux[i] == 1)) 
                    {
                        al_draw_filled_circle(470 - (i * 60), 70, RADIO_CIRCULO, al_color_name("darkred"));
                        al_draw_circle(470 - (i * 60), 70, RADIO_CIRCULO, al_color_name("black"), 2);
                        bitClr('A', i);
                    }
                    else if ((bitGet('A', i) == 0) && (aux[i] == 1))
                    {
                        al_draw_filled_circle(470 - (i * 60), 70, RADIO_CIRCULO, al_color_name("red"));
                        al_draw_circle(470 - (i * 60), 70, RADIO_CIRCULO, al_color_name("black"), 2);
                        bitSet('A', i);
                    } 
                        
                } 
                   
                al_flip_display();
                    
            }
    
        }
    }
       
    
    //Liberamos la memoria
    al_destroy_display(display);            
    al_destroy_event_queue(event_queue);
    al_destroy_timer(timer);
    al_destroy_sample(audio_sample_on);
    al_destroy_sample(audio_sample_off);    
    al_destroy_sample(audio_sample_start);
    al_destroy_sample(audio_sample_toggle);
    al_destroy_sample(audio_sample_blinking);
    al_destroy_sample(audio_sample_shutdown);
   
    return (EXIT_SUCCESS);
}
